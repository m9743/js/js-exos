window.onload = function() {



    function getPhonem() {
        let vowels = ['a', 'e', 'i', 'o', 'u', 'y'];
        let consonants = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'];

        let vowel = vowels[Math.floor(Math.random()*vowels.length)];
        let consonant = consonants[Math.floor(Math.random()*consonants.length)];

        let random = Math.random();

        if(random < 0.5) {
            return consonant + vowel;
        }
        else {
            return vowel + consonant;
        }
    }

    function getWord(nb) {
        let word = "";

        for(let i=0; i < nb; i++) {
            word += getPhonem();
        }

        return word;
    }

    function getPhrase(nbPhrase, nbWord) {
        let phrase = "";

        for(let i=0; i < nbPhrase; i++) {
            phrase += getWord(nbWord) + " ";
        }

        return phrase;
    }

    console.log(getPhrase(4, 3));




}