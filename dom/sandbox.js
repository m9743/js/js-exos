window.onload = function () {
    /**
     * create a red-blue gradient on a table color
     * @param {number} row 
     * @param {number} column 
     */
    function createTable(row, column) {

        let tb = document.createElement("tb");
        const RED = 0;
        const BLUE = 255;

        for(let i=0; i<row; i++) {
            let tr = document.createElement("tr");

            for(let j=0; j<column; j++) {
                let td = document.createElement("td");
                let div = document.createElement("div");

                div.style.backgroundColor = `rgb(${RED+(255/row)*(i+1)}, 0, ${BLUE-(255/row)*(i+1)})`;

                div.innerHTML = `${i}, ${j}`;

                td.appendChild(div);
                tr.appendChild(td);
            }
            tb.appendChild(tr);
        }

        document.body.appendChild(tb);
    }

    createTable(10,20);


}