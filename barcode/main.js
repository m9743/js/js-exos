window.onload = function() {

    /* CANVAS PARAMETERS */

    let canvas = document.getElementById("myCanvas");

    // put fullscreen
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    // remove scrollbars
    document.body.style.overflow = 'hidden';

    // set to 2d context to use geometric form
    let ctx = canvas.getContext("2d");

    let barcodeLength = 0;

    while(barcodeLength < window.innerWidth) {
        
        
        let width = Math.random()*100;
        
        
        ctx.fillRect(barcodeLength, 0, width, window.innerHeight);


        let offsetWidth = Math.random()*100;

        barcodeLength += (width + offsetWidth);
        console.log(barcodeLength);
    }
}
    
