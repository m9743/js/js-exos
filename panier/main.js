window.onload = function() {
    
    let form = document.getElementById("form");
    form.addEventListener("submit", addProduct);

    
    let div = document.createElement("div");
    div.setAttribute("class", "col-sm-6 text-center");
    let div4 = document.createElement("div");
    div4.setAttribute("class", "row m-3");

    div.appendChild(div4);
    document.getElementsByClassName("row")[0].appendChild(div);


    let basketContent = "";
    let sumPrice = 0;
    let removeProduct = false;
    div.addEventListener("click", fillBasket);
    div.addEventListener("click", deleteProduct);


    let div2 = document.createElement("div");
    div2.setAttribute("class", "row m-3");
    let div3 = document.createElement("div");
    div3.setAttribute("class", "col text-center");

    div2.appendChild(div3);
    document.getElementsByClassName("left-side")[0].appendChild(div2);

    
    buildProduct();
    buildBasket();


    



    /**
     * add product to the stock and refresh it
     * @param {*} event 
     */
    function addProduct(event) {
        event.preventDefault(); // block l'event pour ne pas changer de page

        let regexRef = /^[A-Z]{2}[-][0-9]{3}$/;
        let regexName = /^[a-zA-Z]/;
        let regexDesc = /^[a-zA-Z]/;
        let regexQtY = /^[0-9]/;
        let regexPrice = /^[0-9]/;

        if(!form.elements["ref"].value.match(regexRef)) return alert("Oy la ref !");
        if(!form.elements["name"].value.match(regexName)) return alert("Oy le nom !");
        if(!form.elements["desc"].value.match(regexDesc)) return alert("Oy la description !");
        if(!form.elements["qty"].value.match(regexQtY)) return alert("Oy la quantité !");
        if(!form.elements["price"].value.match(regexPrice)) return alert("Oy le prix !");

        let product = {
            ref : form.elements["ref"].value,
            name : form.elements["name"].value,
            desc : form.elements["desc"].value,
            qty : form.elements["qty"].value,
            price : form.elements["price"].value
        }

        localStorage.setItem("product" + localStorage.length, JSON.stringify(product));
        buildProduct();
    }


    /**
     * build and display all products from the stock
     */
    function buildProduct() {
        let table = "<table class='table table-striped'>";
        
            table += "<thead class='thead-dark'>";
                table += "<tr>";
                    table += "<th>REF.</th>";
                    table += "<th>NOM</th>";
                    table += "<th>DESCRIPTION</th>";
                    table += "<th>QTE</th>";
                    table += "<th>PRIX</th>";
                table += "</tr>";
            table += "</thead>"
    
            table += "<tbody>";
                
            for (let i=0; i<localStorage.length; i++) {
                table += "<tr>";
                
                let pdt = JSON.parse(localStorage.getItem(localStorage.key(i)));

                for(let key in pdt) {  
                    table += "<td>";
                        table += pdt[key];
                    table += "</td>";
                }

                // ajouter supprimer button
                table += "<td>";
                    table += "<div class='input-group'>";

                        table += "<input id='inputQty" + localStorage.key(i) +"' type='text' class='form-control' aria-describedby='quantity (add or remove)'></input>";
                        
                        table += "<div class='input-group-append'>";
                            table += "<button id='" + localStorage.key(i) + "' class='btn btn-primary mr-1' type='submit'>Ajouter</button>";
                            table += "<button class='btn btn-danger' type='submit'>Supprimer</button>";
                        table += "</div>"

                    table += "</div>";
                table += "</td>"; 

                
                //table += "<td><button id='"+ localStorage.key(i) + "-rm' class='btn btn-primary'>supprimer</button></td>";
                //table += "<td><button id='"+ localStorage.key(i) + "' class='btn btn-primary btn-sm'>ajouter</button></td>";    
                
                table += "</tr>";
            }

            table += "</tbody>";
        table += "</table>";

        div4.innerHTML = table;



    }

    

    /**
     * build and display the basket with our items
     */
    function buildBasket() {
        let table = "<table class='table overflow-y: hidden'>";
        
            table += "<thead class='thead-dark'>";
                table += "<tr>";
                    table += "<th colspan='4'>PANIER</th>";
                table += "</tr>";

                table += "<tr>";
                    table += "<th>NOM</th>";
                    table += "<th>QUANTITE</th>";
                    table += "<th>PRIX</th>";
                    table += "<th>PRIX DU LOT</th>";
                table += "</tr>";
            table += "</thead>";

            table += "<tbody>";
                table += basketContent;
            table += "</tbody>";

            table += "<tr>";
                table += "<td colspan='3'></td>"
                table += "<td class='bg-primary text-white'>";
                    table += "TOTAL : " + sumPrice + " €";
                table += "</td>";
            table += "</tr>";


        table += "</table>";

        div3.innerHTML = table;
    }

    /**
     * fill and refresh the Basket. Sum all prices
     * @param {Event} event 
     */
    function fillBasket(event) {
        for (let i=0; i<localStorage.length; i++) {
            if(event.target.id === localStorage.key(i)) {

                let productSelected = JSON.parse(localStorage.getItem(localStorage.key(i)));

                let price = parseInt(productSelected.price);
                let qtyBougth = document.getElementById("inputQty" + localStorage.key(i)).value;
                let qtyStock = productSelected.qty;
                
                // check input condition : number and enough quantity in stock
                let regexQtY = /^[0-9]/;
                if(!qtyBougth.match(regexQtY)) {
                    return alert("Oy la quantité !");
                }
                else if(parseInt(qtyBougth) > parseInt(qtyStock)) {
                    return alert("Oy je n'ai pas tout ça moi !");
                }

                // update new quantity of the product
                let newQtyStock = parseInt(qtyStock) - parseInt(qtyBougth); 

                productSelected.qty = newQtyStock.toString();
                localStorage.setItem(localStorage.key(i), JSON.stringify(productSelected));

                removeProduct = true;


                let priceBundle = qtyBougth * price;

                basketContent += "<tr>";
                basketContent += "<td>" + productSelected.name + "</td>";
                basketContent += "<td>" + qtyBougth + " pc(s)" + "</td>";
                basketContent += "<td>" + price + "€/pc" + "</td>";
                basketContent += "<td>" + priceBundle + "€" + "</td>";
                basketContent += "</tr>";

                sumPrice += priceBundle;
        
            }
        } 

        buildBasket();        
    }
    
    /**
     * delete product in the stock when we add it in the basket and refresh
     * @param {Event} event 
     */
    function deleteProduct(event) {
        for (let i=0; i<localStorage.length; i++) {
            if(event.target.id === localStorage.key(i) && removeProduct) {            
                //localStorage.removeItem(localStorage.key(i));

                buildProduct();
                break;
            }
        }

    }
}


/*
 localStorage ou sessionStorage + 
 
 setItem(clé,valeur) : stocke une paire clé/valeur
 getItem(clé) : retourne la valeur associée à une clé
 removeItem(clé) : supprime la paire clé/valeur en indiquant le nom de la clé
 key(index): retourne la clé stockée à l'index spécifié
 clear(): efface toutes les paires

     localStorage.setItem("colour", "green");
    localStorage.couleur = "rouge";

    console.log(localStorage.getItem("couleur"));
 
 */
