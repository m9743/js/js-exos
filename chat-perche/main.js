window.onload = function() {

    // create a canvas in the body and set it to full screen
    let canvas = document.createElement("canvas");
    document.body.appendChild(canvas);

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    document.body.style.overflow = 'hidden';
    document.body.style.margin = 0;

    // define the 2D context
    let ctx = canvas.getContext("2d");

    // static square
    let sSquare = {
        x : 300,
        y : 300,
        wh : 100,
        ht : 100,
        color : "blue",

        hit : function (color) {
            sSquare.color = color;
        }
    }

    // moving square
    let mSquare = {
        x : 0,
        y : 0,
        wh : 100,
        ht : 100,
        color : "red",
        speed : 1
    }

    // static hitbox
    let sHitbox = {
        x : 350,
        y : 350,
        r : 10,
        color : "yellow"
    }

    // moving hitbox
    let mHitbox = {
        x : 50,
        y : 50,
        r : 10,
        speed : 1,
        color : "yellow"
    }

    // keyboard parameters
    let keyboard = {
        right : false,
        left : false,
        up : false,
        down : false
    }

    document.addEventListener("keydown", pressButton, false);
    document.addEventListener("keyup", releaseButton, false);

    setInterval(render, 1);
    


    /*      FUNCTIONS       */

    function render() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);     
        drawSSquare();
        drawMSquare();
        drawSHitbox();
        drawMHitbox();

        moveSSquare();
        moveSHitbox();

        collide();

    }

    function drawSSquare() {
        ctx.beginPath();
        ctx.fillStyle = sSquare.color;
        ctx.fillRect(sSquare.x, sSquare.y, sSquare.wh, sSquare.ht);
        ctx.closePath();
    }

    function drawMSquare() {
        ctx.beginPath();
        ctx.fillStyle = mSquare.color;
        ctx.fillRect(mSquare.x, mSquare.y, mSquare.wh, mSquare.ht);
        ctx.closePath();
    }

    function drawSHitbox() {
        ctx.beginPath();
        ctx.arc(sHitbox.x, sHitbox.y, sHitbox.r, 0, 2*Math.PI);
        ctx.fillStyle = sHitbox.color;
        ctx.fill();
        ctx.closePath();
    }

    function drawMHitbox() {
        ctx.beginPath();
        ctx.arc(mHitbox.x, mHitbox.y, mHitbox.r, 0, 2*Math.PI);
        ctx.fillStyle = mHitbox.color;
        ctx.fill();
        ctx.closePath();
    }


    function pressButton(e) {
        if(e.key === "ArrowRight") {
            keyboard.right = true;
        }
        if(e.key === "ArrowLeft") {
            keyboard.left = true;
        }
        if(e.key === "ArrowUp") {
            keyboard.up = true;
        }
        if(e.key === "ArrowDown") {
            keyboard.down = true;
        }
    }

    function releaseButton(e) {
        if(e.key === "ArrowRight") {
            keyboard.right = false;
        }
        if(e.key === "ArrowLeft") {
            keyboard.left = false;
        }
        if(e.key === "ArrowUp") {
            keyboard.up = false;
        }
        if(e.key === "ArrowDown") {
            keyboard.down = false;
        }
    }

    function moveSSquare() {
        if(keyboard.right && (mSquare.x+mSquare.wh) < canvas.width) {
            mSquare.x += mSquare.speed;
        }
        if(keyboard.left && (mSquare.x) > 0) {
            mSquare.x -= mSquare.speed;
        }
        if(keyboard.up && (mSquare.y) > 0) {
            mSquare.y -= mSquare.speed;
        }
        if(keyboard.down && (mSquare.y+mSquare.ht) < canvas.height) {
            mSquare.y += mSquare.speed;
        }
    }

    function moveSHitbox() {
        if(keyboard.right && (mSquare.x+mSquare.wh) < canvas.width) {
            mHitbox.x += mHitbox.speed;     
        }
        if(keyboard.left && (mSquare.x) > 0) {
            mHitbox.x -= mHitbox.speed;

        }
        if(keyboard.up && (mSquare.y) > 0) {
            mHitbox.y -= mHitbox.speed;
        }
        if(keyboard.down && (mSquare.y+mSquare.ht) < canvas.height) {
            mHitbox.y += mHitbox.speed;
        }
    }

    function collide() {
        let distance = Math.sqrt(Math.pow((mHitbox.x - sHitbox.x), 2) + Math.pow((mHitbox.y - sHitbox.y), 2));
        if (distance <= (mHitbox.r + sHitbox.r)) {
            sSquare.hit("green");
        }
        else {
            sSquare.hit("blue");
        }
    }
}